@extends('layouts.master')

@section('title')
Generic Social Network - User Profile
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored" style="padding-top: 20px;">Search results</div>
		
		@foreach($users as $user)
			<div class="create small">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						<a href="/2503ict-assign2/public/user/{{{ $user->id }}}">{{{ $user->name }}}</a>
					</div>
				</div>
			</div>
		@endforeach
		
	</div>
@stop