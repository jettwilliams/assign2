@extends('layouts.master')

@section('title')
Generic Social Network - Log in
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Log in</div>
		
		<div class="create">
			{{ Form::open(array('action' => 'UserController@login')) }}
			<!--<form method="post" action="/2503ict-assign2/public/action/edit-post">-->
				<input name="email" type="email" placeholder="Email" onkeyup="validateForm()" />
				<input name="password" type="password" placeholder="Password" onkeyup="validateForm()" />
				
				<div style="display: table; margin: 0 auto; margin-top: 6px;">
					<div style="display: table-cell; padding-right: 10px;">
						<div id="submit" class="button" onclick="submitForm()">Log in</div>
					</div>
				</div>
			{{ Form::close() }}
		</div>
		
	</div>
@stop
