@extends('layouts.master')

@section('title')
Generic Social Network - Home
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Posts</div>
		
		@foreach($posts as $post)
			<div class="post">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						<a href="/2503ict-assign2/public/user/{{{ $post->user_id }}}">{{{ $post->user->name }}}</a>
					</div>
				</div>
				<div class="text">
					<a href="/2503ict-assign2/public/post/{{{ $post->id }}}">
						<div class="title">
							{{{ $post->title }}}
						</div>
					</a>
					<div class="message">
						{{{ $post->message }}}
					</div>
					<div class="options">
						@if (Auth::check() && $post->user_id == Auth::user()->id)
							<span class="option"><a href="/2503ict-assign2/public/edit/{{{ $post->id }}}">Edit</a></span>
							<span class="option"><a href="/2503ict-assign2/public/action/delete-post/{{{ $post->id }}}">Delete</a></span>
						@endif
						<span class="option"><a href="/2503ict-assign2/public/post/{{{ $post->id }}}">View Comments ({{{ count($post->comments) }}})</a></span>
					</div>
				</div>
			</div>
		@endforeach
		
		<div class="title colored" style="padding-top: 20px;">New Post</div>
		
		<div class="create">
			
			@if (Auth::check())
				{{ Form::open(array('action' => 'PostController@store')) }}
				<!--{{ Form::text('user', null, ['placeholder' => 'Name', 'onkeyup' => "validateForm()"]) }}-->
					<input name="title" type="text" placeholder="Title" onkeyup="validateForm()" />
					<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()"></textarea>
					Privacy: <select name="privacy">
						<option value="0">Public</option>
						<option value="1" selected>Friends</option>
						<option value="2">Private</option>
					</select>
					<div id="submit" class="button" onclick="submitForm()">Post</div>
			    {{ Form::close() }}
			@else
				<a href="/2503ict-assign2/public/login">Log in</a> or <a href="/2503ict-assign2/public/signup">Sign up</a> to make a post.
			@endif
		</div>
		
	</div>
@stop