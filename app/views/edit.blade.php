@extends('layouts.master')

@section('title')
Generic Social Network - Edit Post
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Edit Post</div>
		
		<div class="create">
			{{ Form::open(array('action' => 'PostController@update')) }}
			<!--<form method="post" action="/2503ict-assign2/public/action/edit-post">-->
				<input name="postid" type="hidden" value="{{{ $post->id }}}" />
				<input name="title" type="text" placeholder="Title" value="{{{ $post->title }}}" onkeyup="validateForm()" />
				<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()">{{{ $post->message }}}</textarea>
				Privacy: <select name="privacy">
					<option value="0"{{{ $post->privacy == 0 ? " selected" : "" }}}>Public</option>
					<option value="1"{{{ $post->privacy == 1 ? " selected" : "" }}}>Friends</option>
					<option value="2"{{{ $post->privacy == 2 ? " selected" : "" }}}>Private</option>
				</select>
				
				<div style="display: table; margin: 0 auto;">
					<div style="display: table-cell; padding-right: 10px;">
						<div id="submit" class="button clickable" onclick="submitForm()">Save</div>
					</div>
					<div style="display: table-cell; padding-left: 10px;">
						<div class="button" onclick="window.history.back()">Cancel</div>
					</div>
				</div>
			{{ Form::close() }}
		</div>
		
	</div>
@stop
