@extends('layouts.master')

@section('title')
Generic Social Network - User Profile
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">User</div>
		
		<div class="post">
			<div class="user">
				<div class="avatar">
					<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
				</div>
				<div class="name">
					<a href="/2503ict-assign2/public/user/{{{ $user->id }}}">{{{ $user->name }}}</a>
				</div>
			</div>
			<div class="text">
				<div class="message">
					Age: {{{ date_diff(date_create($user->birthdate), date_create('now'))->y }}}
				</div>
				<div class="message">
					@if (Auth::check() && $user->id != Auth::user()->id)
						{{--*/ $isfriend = false /*--}}
						@foreach($user->friends as $friend)
							@if ($friend->id == Auth::user()->id)
								{{--*/ $isfriend = true /*--}}
								[Friends] <a href="/2503ict-assign2/public/action/unfriend/{{{ $user->id }}}">Un-friend</a>
							@endif
						@endforeach
						@if (!$isfriend)
							<a href="/2503ict-assign2/public/action/friend/{{{ $user->id }}}">Add Friend</a>
						@endif
					@endif
				</div>
				<div class="options">
					
				</div>
			</div>
		</div>
		
		<div class="title colored" style="padding-top: 20px;">Posts</div>
		
		@foreach($posts as $post)
			<div class="post">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						<a href="/2503ict-assign2/public/user/{{{ $post->user_id }}}">{{{ $post->user->name }}}</a>
					</div>
				</div>
				<div class="text">
					<a href="/2503ict-assign2/public/post/{{{ $post->id }}}">
						<div class="title">
							{{{ $post->title }}}
						</div>
					</a>
					<div class="message">
						{{{ $post->message }}}
					</div>
					<div class="options">
						@if (Auth::check() && $post->user_id == Auth::user()->id)
							<span class="option"><a href="/2503ict-assign2/public/edit/{{{ $post->id }}}">Edit</a></span>
							<span class="option"><a href="/2503ict-assign2/public/action/delete-post/{{{ $post->id }}}">Delete</a></span>
						@endif
						<span class="option"><a href="/2503ict-assign2/public/post/{{{ $post->id }}}">View Comments ({{{ count($post->comments) }}})</a></span>
					</div>
				</div>
			</div>
		@endforeach
		
		<div class="title colored" style="padding-top: 20px;">Friends</div>
		
		@foreach($user->friends as $friend)
			<div class="create small">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						<a href="/2503ict-assign2/public/user/{{{ $friend->id }}}">{{{ $friend->name }}}</a>
					</div>
				</div>
			</div>
		@endforeach
		
	</div>
@stop