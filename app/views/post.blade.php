@extends('layouts.master')

@section('title')
Generic Social Network - View Comments
@stop

@section('content')
	<div class="timeline">
		
		<div class="post">
			<div class="user">
				<div class="avatar">
					<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
				</div>
				<div class="name">
					<a href="/2503ict-assign2/public/user/{{{ $post->user_id }}}">{{{ $post->user->name }}}</a>
				</div>
			</div>
			<div class="text">
				<div class="title">
					{{{ $post->title }}}
				</div>
				<div class="message">
					{{{ $post->message }}}
				</div>
				<div class="options">
					@if (Auth::check() && $post->user_id == Auth::user()->id)
						<span class="option"><a href="/2503ict-assign2/public/edit/{{{ $post->id }}}">Edit</a></span>
						<span class="option"><a href="/2503ict-assign2/public/action/delete-post/{{{ $post->id }}}">Delete</a></span>
					@endif
				</div>
			</div>
		</div>
		
		<div class="title colored" style="padding-top: 20px;">Comments</div>
		Page: 
		￼@for($i=1; $i <= ceil($post->comments()->count()/8); $i++)
		    <a{{{ $page == $i ? ' class=current' : '' }}} href="/2503ict-assign2/public/post/{{{ $post->id }}}?page={{{ $i }}}">{{{ $i }}}</a>
		@endfor
		
		@foreach($post->comments()->offset(8*($page-1))->limit(8)->orderBy('id', 'DESC')->get() as $comment)
			<div class="post">
				<div class="user">
					<div class="avatar">
						<img src="/2503ict-assign2/public/images/avatar.svg" width="60" height="60" alt="Avatar" />
					</div>
					<div class="name">
						<a href="/2503ict-assign2/public/user/{{{ $comment->user_id }}}">{{{ $comment->user->name }}}</a>
					</div>
				</div>
				<div class="text">
					<div class="message">
						{{{ $comment->message }}}
					</div>
					<div class="options">
						@if (Auth::check() && $comment->user_id == Auth::user()->id)
							<span class="option"><a href="/2503ict-assign2/public/action/delete-comment/{{{ $comment->id }}}">Delete</a></span>
						@endif
					</div>
				</div>
			</div>
		@endforeach
		
		<div class="title colored" style="padding-top: 20px;">New Comment</div>
		
		<div class="create">
			@if (Auth::check())
				{{ Form::open(array('action' => 'CommentController@store')) }}
					<input name="postid" type="hidden" value="{{{ $post->id }}}" />
					<textarea name="message" placeholder="Message" rows="4" onkeyup="validateForm()"></textarea>
					<div id="submit" class="button" onclick="submitForm()">Post</div>
				{{ Form::close() }}
			@else
				<a href="/2503ict-assign2/public/login">Log in</a> or <a href="/2503ict-assign2/public/signup">Sign up</a> to make a comment.
			@endif
		</div>
		
	</div>
@stop