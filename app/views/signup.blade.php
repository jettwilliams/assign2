@extends('layouts.master')

@section('title')
Generic Social Network - Sign up
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Sign up</div>
		
		<div class="create">
			{{ Form::open(array('action' => 'UserController@store')) }}
			<!--<form method="post" action="/2503ict-assign2/public/action/edit-post">-->
				<input name="fname" type="text" placeholder="Name" onkeyup="validateForm()" />
				<input name="email" type="email" placeholder="Email" onkeyup="validateForm()" />
				<input name="dob" type="date" onkeyup="validateForm()" onchange="validateForm()" />
				<input name="pass1" type="password" placeholder="Password" onkeyup="validateForm()" />
				<input name="pass2" type="password" placeholder="Re-enter password" onkeyup="validateForm()" />
				
				<div style="display: table; margin: 0 auto; margin-top: 6px;">
					<div style="display: table-cell; padding-right: 10px;">
						<div id="submit" class="button" onclick="submitForm()">Sign up</div>
					</div>
				</div>
			{{ Form::close() }}
		</div>
		
	</div>
@stop
