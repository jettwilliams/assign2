<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="/2503ict-assign2/public/main.css">
		
		<script>
			function validateForm(formNo)
			{
				formNo = typeof formNo !== 'undefined' ? formNo : 1;
				var form = document.forms[formNo];
				for (var i=0; i<form.length; i++)
				{
					if ((form[i].value.trim() == '' || form[i].value.trim() == null) && form[i].getAttribute('type') != 'file')
					{
						document.getElementById("submit"+(formNo == 1 ? "" : formNo)).className = "button";
						return false;
					}
				}
				document.getElementById("submit"+(formNo == 1 ? "" : formNo)).className = "button clickable";
				return true;
			}
			
			function submitForm(formNo)
			{
				formNo = typeof formNo !== 'undefined' ? formNo : 1;
				if (validateForm(formNo))
				{
					document.forms[formNo].submit(); 
				}
				return false;
			}
		</script>
	</head>
	<body>
		<nav>
			<span class="logo">
				<a href="/2503ict-assign2/public/">Generic Social Network</a>
			</span>
			<span>
				{{ Form::open(array('action' => 'UserController@search')) }}
					<input type="text" name="query" placeholder="Search" style="width: 200px; padding: 3px;" />
					<!--<input type="submit" style="font-size: 16px;" value="&#9906;" />-->
				{{ Form::close() }}
			</span>
			<span class="nav">
				<a href="/2503ict-assign2/public/docs">Documentation</a>
				@if (Auth::check())
					<a href="/2503ict-assign2/public/user/{{ Auth::user()->id }}">{{ Auth::user()->name }}</a>
					<a href="/2503ict-assign2/public/account">Settings</a>
					<a href="/2503ict-assign2/public/action/logout">Log out</a>
				@else
				    <a href="/2503ict-assign2/public/login">Log in</a>
				    <a href="/2503ict-assign2/public/signup">Sign up</a>
				@endif
					
			</span>
		</nav>
		<main>
            @section('content')
            There is no content on this page.
            @show
		</main>
	</body>
</html>
