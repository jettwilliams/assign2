<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check())
		{
			//-- get friends list
			$friends = Auth::user()->friends;
			$friendslist = '';
			foreach ($friends as $f)
			{
				$friendslist .= $f->id . ',';
			}
			$friendslist = rtrim($friendslist, ',');
			//$friendslist .= ')';
			$posts = Post::whereRaw("privacy=0 or user_id=? or (user_id in (?) and privacy=1)",array(Auth::user()->id, $friendslist))->orderBy('id', 'DESC')->get();
		}
		else
		{
			$posts = Post::whereRaw("privacy=0")->orderBy('id', 'DESC')->get();
		}
		return View::make('home', compact('posts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//-- Get variables through form data
		$user = Auth::user()->id;
		$title = Input::get('title');
		$message = Input::get('message');
		$privacy = Input::get('privacy');
		
		$result = insertPost($user, $title, $message, $privacy);
	
		if ($result) 
		{
			return Redirect::to(url('.')); //-- Go to home page
		} 
		else
		{
			die("Please fill out all form fields."); //-- If bypass javascript validation
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (isset($_GET['page']))		//-- Get page for comment pagination
			$page = $_GET['page'];
		else
			$page = 1;
		$post = Post::find($id);
		return View::make('post', compact('post'), compact('page'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		return View::make('edit', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//-- Get variables through form data
		$id = Input::get('postid');
		$title = Input::get('title');
		$message = Input::get('message');
		$privacy = Input::get('privacy');
		
		$result = editPost($id, $title, $message, $privacy);
		
		if ($result) 
		{
			return Redirect::to(url('post/'.$id)); //-- Go to posts / comments page
		} 
		else
		{
			die("Please fill out all form fields."); //-- If bypass javascript validation
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$result = deletePost($id);
	
		if ($result) 
		{
			return Redirect::to(url('.')); //-- Go to home page
		} 
		else
		{
			die("Error deleting post."); //-- If bypass javascript validation
		}
	}
}

function insertPost($user, $title, $message, $privacy) 
{
	$id = 0;
	
	//-- Make sure form input not interpreted as HTML
	$user = htmlspecialchars($user);
	$title = htmlspecialchars($title);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($user != '' && $title != '' && $message != '' && $privacy != '')		//-- check input not blank
	{
		$post = new Post;

		$post->user_id = $user;
		$post->title = $title;
		$post->message = $message;
		$post->privacy = $privacy;
		
		$post->save();
		
		$id = 1;
		
		//$id = DB::getPdo()->lastInsertId(); //-- Get id of last inserted row
	}
	
	return $id;
}

function editPost($id, $title, $message, $privacy) 
{
	$result = 0;
	
	//-- Make sure form input not interpreted as HTML
	$title = htmlspecialchars($title);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($id != '' && $title != '' && $message != '' && $privacy != '')
	{
		$post = Post::find($id);
		
		if ($post->user_id == Auth::user()->id) 	//-- Check logged in as user
		{
			$post->title = $title;
			$post->message = $message;
			$post->privacy = $privacy;
			
			$post->save();
			
			$result = 1;
		}
		else
		{
			$result = 0;
		}
	}
	
	return $result;
}

function deletePost($id)
{
	$post = Post::find($id);
	if ($post->user_id == Auth::user()->id)		//-- Check logged in as user
	{
		$post->comments()->delete();
		$post->delete();

		return 1;
	}
	else
	{
		return 0;
	}
}