<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($search = '')
	{
		$users = User::whereRaw("name like ?",array('%'.$search.'%'))->get();
		return View::make('search', compact('users'));
	}
	
	public function search()
	{
		$query = Input::get('query');
		return Redirect::to(url('search/' . $query));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('signup');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//-- Get variables through form data
		$fname = Input::get('fname');
		$email = Input::get('email');
		$dob = Input::get('dob');
		$pass1 = Input::get('pass1');
		$pass2 = Input::get('pass2');
		
		$result = insertUser($fname, $email, $dob, $pass1, $pass2);
	
		if ($result) 
		{
			$password = $pass1;
			Auth::attempt(compact('email', 'password'));
			return Redirect::to(url('account')); //-- Go to settings page
		} 
		else
		{
			die("Please fill out all form fields correctly."); //-- If bypass javascript validation
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		if (Auth::check())
		{
			
			$friends = Auth::user()->friends;
			$friendslist = '';
			foreach ($friends as $f)
			{
				$friendslist .= $f->id . ',';
			}
			$friendslist = rtrim($friendslist, ',');
			//$friendslist .= ')';
			$posts = Post::whereRaw("user_id=? and (privacy=0 or user_id=? or (user_id in (?) and privacy=1))",array($user->id,Auth::user()->id, $friendslist))->orderBy('id', 'DESC')->get();
		}
		else
		{
			$posts = Post::whereRaw("user_id=? and privacy=0",array($user->id))->orderBy('id', 'DESC')->get();
		}
		return View::make('user', compact('user'), compact('posts'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		return View::make('account');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//-- Get variables through form data
		$name = Input::get('fname');
		$dob = Input::get('dob');
		$email = Input::get('email');
		if (Input::hasFile('image')) {
			$image = Input::file('image');	
		}
		
		
		$result = editUser($name, $dob, $email);
		
		if ($result) 
		{
			return Redirect::to(url('user/' . Auth::user()->id)); //-- Go to posts / comments page
		} 
		else
		{
			die("Please fill out all form fields correctly."); //-- If bypass javascript validation
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function loginform()
	{
		return View::make('login');
	}
	
	function login()
	{
		$input = Input::all();
		$email = $input['email'];
		$password = $input['password'];
		if (Auth::attempt(compact('email', 'password')))
		{
			return Redirect::to(url('.')); //-- Go to home page
		}
		else
		{
			return Redirect::to(URL::previous());
		}
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::to(url('.'));
	}
	
	public function friend($id)
	{
		if (Auth::check())		//-- User logged in
		{
			$user = Auth::user();
			$user->friends()->attach($id);
			$user->save();
			
			$user2 = User::find($id);		//-- Add two friend rows
			$user2->friends()->attach($user->id);
			$user2->save();
		}
		return Redirect::to(url('user/' . $id));
	}
	
	public function unfriend($id)
	{
		if (Auth::check())
		{
			$user = Auth::user();
			$user->friends()->detach($id);
			$user->save();
			
			$user2 = User::find($id);
			$user2->friends()->detach($user->id);
			$user2->save();
		}
		return Redirect::to(url('user/' . $id));
	}

}

function insertUser($fname, $email, $dob, $pass1, $pass2)
{
	$id = 0;
	
	//-- Make sure form input not interpreted as HTML
	$fname = htmlspecialchars($fname);
	$email = htmlspecialchars($email);
	$dob = htmlspecialchars($dob);
	$pass1 = htmlspecialchars($pass1);
	$pass2 = htmlspecialchars($pass2);
	
	//-- Make sure form elements not blank
	if ($fname != '' && $email != '' && $dob != '' && $pass1 != '' && $pass1 == $pass2 && User::whereRaw("email=?",array($email))->get()->isEmpty())
	{
		$user = new User;

		$user->name = $fname;
		$user->email = $email;
		$user->birthdate = $dob;
		$user->password = Hash::make($pass1);
		
		$user->save();
		
		$id = 1;
		
		//$id = DB::getPdo()->lastInsertId(); //-- Get id of last inserted row
	}
	
	return $id;
}

function editUser($name, $dob, $email)
{
	$result = 0;
	
	//-- Make sure form input not interpreted as HTML
	$name = htmlspecialchars($name);
	$dob = htmlspecialchars($dob);
	$email = htmlspecialchars($email);
	
	//-- Make sure form elements not blank
	if (Auth::check() && $name != '' && $dob != '' && $email != '')
	{
		$user = User::find(Auth::user()->id);
		
		$user->name = $name;
		$user->email = $email;
		$user->birthdate = $dob;
		
		$user->save();
		
		$result = 1;
	}
	
	return $result;
}