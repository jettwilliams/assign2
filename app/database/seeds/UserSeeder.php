<?php

class UserSeeder extends Seeder {
    public function run()
    {
        $numbers = array('One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten');
        for ($i=1; $i<=10; $i++)
        {
            $user = new User;
            $user->name = 'Tester ' . $numbers[$i-1];
            $user->email = 't' . $i . '@test.com';
            $user->password = '$2y$10$ugjOyszmRu.clfyhbuCNs.vpyw4SGKmoP0TH54BNVA.xQeXzDzgRK';
            $user->birthdate = ($i + 1990) . '-05-22';
            $user->save();
        }
    }
}