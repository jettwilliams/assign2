<?php

class PostSeeder extends Seeder {
    public function run()
    {
        $privacy = array('Public','Friends','Private');
        for ($i=1; $i<=10; $i++)
        {
            for ($j=0; $j<2; $j++)
            {
                $post = new Post;
                $post->user_id = 1+(($i-1)*$j);
                $post->title = 'Test Post ' . $i;
                $post->message = 'This is a test post with privacy: ' . $privacy[($i-1) % 3];
                $post->privacy = ($i-1) % 3;
                $post->save();
            }
        }
    }
}